$( document ).ready(function() {
          
    function setCookie(c_name,c_value)
    {
        document.cookie=c_name + "=" + c_value;
    }
    function getCookie(c_name)
    {
        var c_value = document.cookie;
        var c_start = c_value.indexOf(" " + c_name + "=");
        if (c_start == -1)
          {
             c_start = c_value.indexOf(c_name + "=");
          }
        if (c_start == -1)
          {
             c_value = null;
          }
        else
          {
            c_start = c_value.indexOf("=", c_start) + 1;
            var c_end = c_value.indexOf(";", c_start);
            if (c_end == -1)
          {
          c_end = c_value.length;
        }
          c_value = unescape(c_value.substring(c_start,c_end));
        }
        return c_value;
    }            
    var total = getCookie("total") ? parseInt(getCookie("total")) : 0;
    var record = getCookie("record") ? parseInt(getCookie("record")) : 0;
    
    
    var docW = $( document ).width()-5;
    var docH = $( document ).height() - 5;    
    var cnvW = docW-docW%10;
    var cellW = cnvW/10;
    var cnvH = docH - docH%cellW;    
    var mar = (docW - cnvW) / 2;
    var w = 10;
    var h = cnvH / cellW +1;
    cnvH = h * cellW;
    //$("#cnv").css("width",cnvW);
    document.getElementById('cnv').width = cnvW;
    document.getElementById('cnv').height = cnvH;
    $("#cnv").css("margin-top",-cellW);
    $("#cnv").css("margin-left",mar);
   // $(".topPanel").css("height",$( document ).height());
    console.log(mar);
   // alert($( window ).width());
   var w = 10;
   var h = cnvH / cellW;
    
function PaintNextFigure(type,version,cnv){
    this.type=type;
    this.cnv=document.getElementById(cnv).getContext("2d");
    this.x=2;
    this.y=2;
    this.color = '';
    this.cellWidth=12;    
    this.w=5;
    this.h=5;
    this.version=version;
    
    this.rows = new Array();
    for (var i=0;i<this.h;i++){
        this.rows[i] = new Array();
        for (var j=0;j<this.w;j++){
            this.rows[i][j] = 0;
        }
    }
        
    this.coordinates = new Array();
    switch (this.type){
        case 'i':
            this.coordinates[0] = [{x:0,y:0},{x:0,y:-1},{x:0,y:1},{x:0,y:2}];
            this.coordinates[1] = [{x:0,y:0},{x:1,y:0},{x:-1,y:0},{x:-2,y:0}];
            this.color='#FFDE99';
        break;
        case 'l':
            this.coordinates[0] = [{x:0,y:0},{x:0,y:-1},{x:0,y:1},{x:1,y:1}];
            this.coordinates[1] = [{x:0,y:0},{x:1,y:0},{x:-1,y:0},{x:-1,y:1}];
            this.coordinates[2] = [{x:0,y:0},{x:0,y:-1},{x:-1,y:-1},{x:0,y:1}];
            this.coordinates[3] = [{x:0,y:0},{x:-1,y:0},{x:1,y:0},{x:1,y:-1}];            
            this.color='#FFCE6B';
        break;
        case 'j':
            this.coordinates[0] = [{x:0,y:0},{x:0,y:-1},{x:0,y:1},{x:-1,y:1}];
            this.coordinates[1] = [{x:0,y:0},{x:-1,y:0},{x:-1,y:-1},{x:1,y:0}];
            this.coordinates[2] = [{x:0,y:0},{x:0,y:-1},{x:1,y:-1},{x:0,y:1}];
            this.coordinates[3] = [{x:0,y:0},{x:-1,y:0},{x:1,y:0},{x:1,y:1}];            
            this.color='#FFC144';
        break;
        case 'o':
            this.coordinates[0] = [{x:0,y:0},{x:1,y:0},{x:0,y:-1},{x:1,y:-1}];            
            this.color='#FFB725';
        break;
        case 's':
            this.coordinates[0] = [{x:0,y:0},{x:1,y:0},{x:0,y:1},{x:-1,y:1}];
            this.coordinates[1] = [{x:0,y:0},{x:0,y:-1},{x:1,y:0},{x:1,y:1}];            
            this.color='#FFAD09';
        break;
        case 'z':
            this.coordinates[0] = [{x:0,y:0},{x:-1,y:0},{x:0,y:1},{x:1,y:1}];
            this.coordinates[1] = [{x:0,y:0},{x:0,y:-1},{x:-1,y:0},{x:-1,y:1}];            
            this.color='#D79000';
        break;
        case 't':
            this.coordinates[0] = [{x:0,y:0},{x:1,y:0},{x:-1,y:0},{x:0,y:-1}];
            this.coordinates[1] = [{x:0,y:0},{x:1,y:0},{x:0,y:-1},{x:0,y:1}];
            this.coordinates[2] = [{x:0,y:0},{x:-1,y:0},{x:1,y:0},{x:0,y:1}];
            this.coordinates[3] = [{x:0,y:0},{x:0,y:-1},{x:0,y:1},{x:-1,y:0}];            
            this.color='#DAB05C';
        break;
    } 
    
    
    this.paintGrig = function(){
        for(var i=0; i<=6; i++){
            this.cnv.beginPath();
            this.cnv.strokeStyle = "rgba(255,133,0,0.5)";
            this.cnv.lineWidth = 1;
            this.cnv.moveTo(0,i*this.cellWidth);
            this.cnv.lineTo(this.cellWidth*this.w,i*this.cellWidth);
            this.cnv.closePath();
            this.cnv.stroke();
        }
        for(var i=0; i<=6; i++){
            this.cnv.beginPath();
            this.cnv.strokeStyle = "rgba(255,133,0,0.5)"; 
            this.cnv.moveTo(i*this.cellWidth,0);
            this.cnv.lineTo(i*this.cellWidth,this.cellWidth*this.h);
            this.cnv.closePath(); 
            this.cnv.stroke();
        }
    }
    this.paintGrig();
    
      this.print = function(){
        for (var i in this.coordinates[this.version]){            
                this.rows[this.y+this.coordinates[this.version][i].y][this.x+this.coordinates[this.version][i].x] = this.color;                       
        }        
    }
     this.print ();

    this.clear = function(){
                    this.cnv.beginPath();     
                    this.cnv.clearRect ( 0 , 0 , 200 , 200 );
                    this.cnv.strokeStyle = "rgba(0, 0, 0, 0.3)";
                    this.cnv.fillStyle = "rgba(0, 0, 0, 0.3)";
                    this.cnv.fillRect(0,0,200,400);
                    this.cnv.closePath(); 
                    this.cnv.stroke();
                    this.cnv.fill();
    }
    this.paint = function(){
        this.clear();
        for (var i=0;i<this.h;i++){
            for (var j=0;j<this.w;j++){
                if (this.rows[i][j] != 0){
                    this.cnv.beginPath();
                    this.cnv.strokeStyle = "black";
                    this.cnv.fillStyle = this.rows[i][j];  
                    //cnv.fillRect((j)*this.cellWidth,(i-1)*this.cellWidth,(j-5)*this.cellWidth,(i)*this.cellWidth);
                    this.cnv.fillRect((j)*this.cellWidth,(i)*this.cellWidth,this.cellWidth,this.cellWidth);                     
                    this.cnv.closePath();
                    this.cnv.stroke();
                    this.cnv.fill();                    
                }
            }            
        }
        this.paintGrig();
    }
    this.paint ();
}            


function Figure(type,version,k){
    this.type=type;
    this.x=4;
    this.y=-2;
    this.color = '';
    this.version=version;
    this.coordinates = new Array();
    
    //for (var i=1;i<=4;i++){this.coordinates[i] = new Array();}
    switch (this.type){
        case 'i':
            this.coordinates[0] = [{x:0,y:0},{x:0,y:-1},{x:0,y:1},{x:0,y:2}];
            this.coordinates[1] = [{x:0,y:0},{x:1,y:0},{x:-1,y:0},{x:-2,y:0}];
            this.color='#FFDE99';
        break;
        case 'l':
            this.coordinates[0] = [{x:0,y:0},{x:0,y:-1},{x:0,y:1},{x:1,y:1}];
            this.coordinates[1] = [{x:0,y:0},{x:1,y:0},{x:-1,y:0},{x:-1,y:1}];
            this.coordinates[2] = [{x:0,y:0},{x:0,y:-1},{x:-1,y:-1},{x:0,y:1}];
            this.coordinates[3] = [{x:0,y:0},{x:-1,y:0},{x:1,y:0},{x:1,y:-1}];            
            this.color='#FFCE6B';
        break;
        case 'j':
            this.coordinates[0] = [{x:0,y:0},{x:0,y:-1},{x:0,y:1},{x:-1,y:1}];
            this.coordinates[1] = [{x:0,y:0},{x:-1,y:0},{x:-1,y:-1},{x:1,y:0}];
            this.coordinates[2] = [{x:0,y:0},{x:0,y:-1},{x:1,y:-1},{x:0,y:1}];
            this.coordinates[3] = [{x:0,y:0},{x:-1,y:0},{x:1,y:0},{x:1,y:1}];            
            this.color='#FFC144';
        break;
        case 'o':
            this.coordinates[0] = [{x:0,y:0},{x:1,y:0},{x:0,y:-1},{x:1,y:-1}];            
            this.color='#FFB725';
        break;
        case 's':
            this.coordinates[0] = [{x:0,y:0},{x:1,y:0},{x:0,y:1},{x:-1,y:1}];
            this.coordinates[1] = [{x:0,y:0},{x:0,y:-1},{x:1,y:0},{x:1,y:1}];            
            this.color='#FFAD09';
        break;
        case 'z':
            this.coordinates[0] = [{x:0,y:0},{x:-1,y:0},{x:0,y:1},{x:1,y:1}];
            this.coordinates[1] = [{x:0,y:0},{x:0,y:-1},{x:-1,y:0},{x:-1,y:1}];            
            this.color='#D79000';
        break;
        case 't':
            this.coordinates[0] = [{x:0,y:0},{x:1,y:0},{x:-1,y:0},{x:0,y:-1}];
            this.coordinates[1] = [{x:0,y:0},{x:1,y:0},{x:0,y:-1},{x:0,y:1}];
            this.coordinates[2] = [{x:0,y:0},{x:-1,y:0},{x:1,y:0},{x:0,y:1}];
            this.coordinates[3] = [{x:0,y:0},{x:0,y:-1},{x:0,y:1},{x:-1,y:0}];            
            this.color='#DAB05C';
        break;
    } 
    this.left = function(){
        this.clear();
        var left = true;
        for (var i in this.coordinates[this.version]){   
            if (this.x-1+this.coordinates[this.version][i].x<0){left=false;}
            try{ 
                if (field[k].rows[this.y+this.coordinates[this.version][i].y][this.x-1+this.coordinates[this.version][i].x] != 0){left = false}            
            } catch(e){}
        }         
        if (left){
            this.clear();
            this.x--;
            this.print();
            field[k].paint();
        } else {this.print}
    }
    this.right = function(){
        this.clear();
        var right = true;
        for (var i in this.coordinates[this.version]){ 
            if (this.x+1+this.coordinates[this.version][i].x>9){right=false;}
            try{ 
                if (field[k].rows[this.y+this.coordinates[this.version][i].y][this.x+1+this.coordinates[this.version][i].x] != 0){right = false}            
            } catch(e){}
        }   
        if (right){
            this.clear();
            this.x++;
            this.print();
            field[k].paint();
        } else {this.print}
    }
    this.nextVersion = function(){
        this.clear();
        var nexVersion = this.version+1;
        if (nexVersion > this.coordinates.length-1){nexVersion = 0;}
        var rotate = true;
        for (var i in this.coordinates[nexVersion]){                        
            if (this.y+this.coordinates[nexVersion][i].y==h){rotate=false;}
            if (this.x+this.coordinates[nexVersion][i].x>9){rotate=false;}
            if (this.x+this.coordinates[nexVersion][i].x<0){rotate=false;}
            else {
                try{ 
                    if (field[k].rows[this.y+this.coordinates[nexVersion][i].y][this.x+this.coordinates[nexVersion][i].x] != 0){
                        rotate = false; 
                    }            
                } catch(e){}
               
            }
        }
        
        if (rotate){
            this.clear();  
            this.version++;
            if (this.version > this.coordinates.length-1){this.version = 0;}
            this.print();
            field[k].paint();
        }
        else{
            this.print();
        }
    }
    this.print = function(){
        for (var i in this.coordinates[this.version]){
            var print=true;
          //  console.log("h="+h);
            //console.log(this.coordinates[this.version][i].x,this.coordinates[this.version][i].y);
            if (this.y+this.coordinates[this.version][i].y<0){print=false;}
            if (this.y+this.coordinates[this.version][i].y>(h-1)){print=false;}
            if (this.x+this.coordinates[this.version][i].x<0){print=false;}
            if (this.x+this.coordinates[this.version][i].x>(h-1)){print=false;}
            //alert();
            //console.log(this.x+this.coordinates[this.version][i].x,' ',this.y+this.coordinates[this.version][i].y,print);
            if (print){
                field[k].rows[this.y+this.coordinates[this.version][i].y][this.x+this.coordinates[this.version][i].x] = this.color;
            }    
        }
        
    }
    this.clear = function(){
       for (var i in this.coordinates[this.version]){
            var print=true;
            if (this.y+this.coordinates[this.version][i].y<0){print=false;}
            if (this.y+this.coordinates[this.version][i].y>(h-1)){print=false;}
            if (this.x+this.coordinates[this.version][i].x<0){print=false;}
            if (this.x+this.coordinates[this.version][i].x>(h-1)){print=false;}
            //console.log(this.x+this.coordinates[this.version][i].x,' ',this.y+this.coordinates[this.version][i].y,print);
            if (print){
                field[k].rows[this.y+this.coordinates[this.version][i].y][this.x+this.coordinates[this.version][i].x] = 0;
            }    
        }
    }
    this.down = function(){        
        this.clear();              
        var fall = true;
        for (var i in this.coordinates[this.version]){                        
            if (this.y+this.coordinates[this.version][i].y+1==h){fall=false} 
            else {
                try{ 
                if (field[k].rows[this.y+this.coordinates[this.version][i].y+1][this.x+this.coordinates[this.version][i].x] != 0){fall = false}            
                } catch(e){}
               
            }
        }
        if (!fall){
                this.print();
                field[k].clearRows();
                $('#score'+k+' span').html(field[k].score);
                 field[k].figureNumber++;
                if (field[k].figusreNumber>300){field[k].figureNumber=0}
                var newFigure = figures[field[k].figureNumber];                 
                figure[k] = new Figure(newFigure.figure,newFigure.version,k);
                
                var nextFigure = figures[field[k].figureNumber+1];                
                PaintNextFigure(nextFigure.figure,nextFigure.version,'nxtF1');
                var arr= "";
                for (var i in field[1].rows) {
                    arr+=field[1].rows[i].toString()+"+";
                }                          
                setCookie("save",arr);     
                setCookie("save-score",field[1].score);               
                if ( field[k].rows[0][3]!=0 || field[k].rows[0][4]!=0 || field[k].rows[0][5]!=0 || field[k].rows[0][6]!=0){
                  
                clearInterval(intrv1);                                      
                   $('#start .text').html('GAME OVER');
                    setCookie("save","");                    
                   start=false;
                }               
        } else {
            this.y++;
            this.print();
            
        }
        //this.print();
        field[k].paint();
        //field[k].logRows();
        //console.log('!');
    }    
    
}         


function playField(cnv){
    this.cnv=document.getElementById(cnv).getContext("2d"); 
    this.cellWidth = cnvW/10;
    this.w=w;
    this.h=h;
    this.score=0;
    this.rows = new Array();
    this.figureNumber=0;
    for (var i=0;i<this.h;i++){
        this.rows[i] = new Array();
        for (var j=0;j<this.w;j++){
            this.rows[i][j] = 0;
        }
    }
    this.randomFigure = function(){
        var f = ['i','l','j','o','s','z','t'];
        var fg = f[Math.floor(Math.random()*7)];        
        switch (fg){
            case 'i':
            case 's':
            case 'z':    
                var version = Math.floor(Math.random()*2);
            break;
            case 'l':
            case 'j':
            case 't':
                var version = Math.floor(Math.random()*4);
            break;
            case 'o':
                var version = 0;
            break;
        }
        return {figure : fg, version : version}
        
    };
    this.logRows = function(){
        for (var i=0;i<this.h;i++){
            var a = '';
            for (var j=0;j<this.w;j++){
                a+= this.rows[i][j];
            }
            console.log(a);
        }    
    }
    this.clear = function(){
                    this.cnv.beginPath();        
                    this.cnv.strokeStyle = "rgba(0,0,0,0)";
                    this.cnv.fillStyle = "rgba(0,0,0,0)";
                    this.cnv.clearRect ( 0,0,this.w*this.cellWidth,this.h*this.cellWidth );
                    this.cnv.fillRect(0,0,this.w*this.cellWidth,this.h*this.cellWidth);
                    this.cnv.closePath(); 
                    this.cnv.stroke();
                    this.cnv.fill();
    }
    this.paint = function(){
        this.clear();
        for (var i=0;i<this.h;i++){
            for (var j=0;j<this.w;j++){
                if (this.rows[i][j] != 0){
                    this.cnv.beginPath();
                    this.cnv.strokeStyle = "black";
                    this.cnv.fillStyle = this.rows[i][j];  
                    //cnv.fillRect((j)*this.cellWidth,(i-1)*this.cellWidth,(j-5)*this.cellWidth,(i)*this.cellWidth);
                    this.cnv.fillRect((j)*this.cellWidth,(i)*this.cellWidth,this.cellWidth,this.cellWidth);                     
                    this.cnv.closePath();
                    this.cnv.stroke();
                    this.cnv.fill();                    
                }
            }            
        }
        this.paintGrig();
    }
    this.paintGrig = function(){
        for(var i=0; i<=this.h; i++){
            this.cnv.beginPath();
            this.cnv.strokeStyle = "rgba(0,0,0,0.1)";
            this.cnv.lineWidth = 1;
            this.cnv.moveTo(0,i*this.cellWidth);
            this.cnv.lineTo(this.cellWidth*this.w,i*this.cellWidth);
            this.cnv.closePath();
            this.cnv.stroke();
        }
        for(var i=0; i<=this.w; i++){
            this.cnv.beginPath();
            this.cnv.strokeStyle = "rgba(0,0,0,0.1)"; 
            this.cnv.moveTo(i*this.cellWidth,0);
            this.cnv.lineTo(i*this.cellWidth,this.cellWidth*this.h);
            this.cnv.closePath(); 
            this.cnv.stroke();
        }
    }
    this.clearRows = function(){
        for (var i=0;i<this.h;i++){
            
            var full = true;
            for (var j=0;j<this.w;j++){
                if (this.rows[i][j]==0) {full = false}
            }
            
            if (full){
               this.score++;   
                total++;
                    setCookie("total",total); 
                    if (field[1].score > record){
                        record=field[1].score;
                        setCookie("record",record);                     
                    }
                    console.log("record: "+record);
                    console.log("total: "+total);
               try{
               window.navigator.vibrate(500);
               }catch(e){}
                for (var j=i;j>0;j--){
                    //this.rows[j] = new Array();
                    for (var k in this.rows[j-1])
                    this.rows[j][k] = this.rows[j-1][k];                                    
                    }
            }
            
        }
        
        if ((this.rows[0][2]!=0) || (this.rows[0][3]!=0) || (this.rows[0][4]!=0) || (this.rows[0][5]!=0)){
           // clearInterval(int);
         //   $('#info').css('display','block');
        }
        
    }
}
var pause=false;
function timer(){
    if (!pause){
        figure[1].down();       
    }
}

var field = new Array();
field[1] = new playField('cnv');

field[1].paintGrig();
var figures = new Array();
var newFigure = figures[1];
var nextFigure = figures[2];
PaintNextFigure('',0,'nxtF1');

var start=false;

function stop(){
    clearInterval(int);
 //   bot.stop;
}

console.log( typeof  getCookie("save"));
if (getCookie("save")){    
    var rows = getCookie("save").split('+');
    for (var i in rows){
        field[1].rows[i] = rows[i].split(',');
    }
    console.log(field[1].rows);
    field[1].paint();
      figures = new Array();
    for (i=-10; i<10000;i++){
        figures[i] = field[1].randomFigure();
    }
    field[1].figureNumber++;
     newFigure = figures[1];
     nextFigure = figures[2];
    // newFigure2 = figures[0];
    // nextFigure2 = figures[2];
    //PaintNextFigure(nextFigure.figure,nextFigure.version,'nxtF1');
    //PaintNextFigure(nextFigure2.figure,nextFigure2.version,'nxtF2');
    //PaintNextFigure('i',0,'nxtF2');
     figure = new Array();
    figure[1] = new Figure(newFigure.figure,newFigure.version,1);
    //figure[2] = new Figure(newFigure2.figure,newFigure2.version,2);
    intrv1 = setInterval(timer,500);
     PaintNextFigure(nextFigure.figure,nextFigure.version,'nxtF1');
    field[1].paint();
    field[1].score=parseInt(getCookie("save-score"));
    $('#score1 span').html(field[1].score);
    start=true;
    pause=true;
    $('#start .text').html('RESUME');
}

$('#start').click(function(){
  if (!start){      
      $('#start .text').html('PAUSE');       
     start=true;
     field = new Array();
    field[1] = new playField('cnv');
    //field[2] = new playField('cnv2');
    field[1].paintGrig();
    //field[2].paintGrig();

     figures = new Array();
    for (i=-10; i<10000;i++){
        figures[i] = field[1].randomFigure();
    }
    field[1].figureNumber++;
     newFigure = figures[1];
     nextFigure = figures[2];
    // newFigure2 = figures[0];
    // nextFigure2 = figures[2];
    //PaintNextFigure(nextFigure.figure,nextFigure.version,'nxtF1');
    //PaintNextFigure(nextFigure2.figure,nextFigure2.version,'nxtF2');
    //PaintNextFigure('i',0,'nxtF2');
     figure = new Array();
    figure[1] = new Figure(newFigure.figure,newFigure.version,1);
    //figure[2] = new Figure(newFigure2.figure,newFigure2.version,2);

    field[1].paint();
    //field[1].logRows();
    //field[2].paint();
    //field[2].logRows();
     //bot = new Bot();
    //bot.generateWay();

       intrv1 = setInterval(timer,500);///////////////////////////////////SPEEEEED!!!!!!!!!!!!!!!!!!!
       //bot.speed=1000-$('#cs').val();
      // bot.nextFigure();
       PaintNextFigure(nextFigure.figure,nextFigure.version,'nxtF1');
      //  PaintNextFigure(nextFigure2.figure,nextFigure2.version,'nxtF2');
  } else {
     pause = pause ? false : true;
     console.log(pause);
     if (pause){$('#start .text').html('RESUME');}else{$('#start .text').html('PAUSE');}
     
  }
});

$(document).keydown(function(e) 
{
     if(e.keyCode==32)
     {
          figure[1].nextVersion();        
     }
     if(e.keyCode==37)
     {
          figure[1].left();           
     } 
     if(e.keyCode==39)
     {
          figure[1].right();       
     } 
     if(e.keyCode==40)
     {
          figure[1].down();       
     } 
});

$("#cnv").click(function(e){
    
         figure[1].nextVersion(); 
    
});
/*
$("#cnv").mousemove(function(e){
    var x = e.pageX;
    var y = e.pageY;
    
    var pos = Math.round(x/cellW);
    if(figure[1].x > pos){
        figure[1].left();       
    } 
    if(figure[1].x < pos){
        figure[1].right();            
    } 

});*/

var oldX=0;
var oldY=0;

var obj = document.getElementById('cnv');

obj.addEventListener('touchstart', function(e) {    
    oldX = e.changedTouches[0].pageX;
    oldY = e.changedTouches[0].pageY;
    //alert("!!!");
}, false);

obj.addEventListener('touchmove', function(event) {
  // If there's exactly one finger inside this element 
  
  if (event.targetTouches.length == 1) {
    var touch = event.targetTouches[0];    
 //   alert(oldX);
    var x = touch.pageX;
    var y = touch.pageY;
    
   
    var pos = Math.round(x/cellW);
    if(oldX > x+cellW){
        figure[1].left(); 
         oldX=x;         
    } 
    if(oldX < x-cellW){
        figure[1].right();   
        oldX=x;        
    }   
    $("#deb").html("oldY="+oldY+", cellW="+cellW);
    if( Math.abs(y-oldY) > cellW*3 ){
        figure[1].down();          
         //oldY=y;
    } 
    
  }
}, false);


});