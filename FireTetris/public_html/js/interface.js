$( document ).ready(function() {

    
    function setCookie(c_name,c_value){                      
                document.cookie=c_name + "=" + c_value;
     }
     function getCookie(c_name)
     {
            var c_value = document.cookie;
            var c_start = c_value.indexOf(" " + c_name + "=");
            if (c_start == -1)
              {
              c_start = c_value.indexOf(c_name + "=");
              }
            if (c_start == -1)
              {
              c_value = null;
              }
            else
              {
              c_start = c_value.indexOf("=", c_start) + 1;
              var c_end = c_value.indexOf(";", c_start);
              if (c_end == -1)
              {
            c_end = c_value.length;
            }
            c_value = unescape(c_value.substring(c_start,c_end));
            }
            return c_value;
      }
      setCookie("name","vasia");
      alert(getCookie("name"));
    
    
    //alert($( document ).width());
    console.log($( document ).width());
    var docW = ($( document ).width() - 70)*0.95;
    
    var cnvW = ""+docW-docW%20+"px";
    console.log(cnvW);
    $("#cnv").css("width",cnvW);
    
function PaintNextFigure(type,version,cnv){
    this.type=type;
    this.cnv=document.getElementById(cnv).getContext("2d");
    this.x=2;
    this.y=2;
    this.color = '';
    this.cellWidth=12;    
    this.w=5;
    this.h=5;
    this.version=version;
    
    this.rows = new Array();
    for (var i=0;i<this.h;i++){
        this.rows[i] = new Array();
        for (var j=0;j<this.w;j++){
            this.rows[i][j] = 0;
        }
    }
        
    this.coordinates = new Array();
    switch (this.type){
        case 'i':
            this.coordinates[0] = [{x:0,y:0},{x:0,y:-1},{x:0,y:1},{x:0,y:2}];
            this.coordinates[1] = [{x:0,y:0},{x:1,y:0},{x:-1,y:0},{x:-2,y:0}];
            this.color='#610B0B';
        break;
        case 'l':
            this.coordinates[0] = [{x:0,y:0},{x:0,y:-1},{x:0,y:1},{x:1,y:1}];
            this.coordinates[1] = [{x:0,y:0},{x:1,y:0},{x:-1,y:0},{x:-1,y:1}];
            this.coordinates[2] = [{x:0,y:0},{x:0,y:-1},{x:-1,y:-1},{x:0,y:1}];
            this.coordinates[3] = [{x:0,y:0},{x:-1,y:0},{x:1,y:0},{x:1,y:-1}];            
            this.color='#61380B';
        break;
        case 'j':
            this.coordinates[0] = [{x:0,y:0},{x:0,y:-1},{x:0,y:1},{x:-1,y:1}];
            this.coordinates[1] = [{x:0,y:0},{x:-1,y:0},{x:-1,y:-1},{x:1,y:0}];
            this.coordinates[2] = [{x:0,y:0},{x:0,y:-1},{x:1,y:-1},{x:0,y:1}];
            this.coordinates[3] = [{x:0,y:0},{x:-1,y:0},{x:1,y:0},{x:1,y:1}];            
            this.color='#5E610B';
        break;
        case 'o':
            this.coordinates[0] = [{x:0,y:0},{x:1,y:0},{x:0,y:-1},{x:1,y:-1}];            
            this.color='#0B610B';
        break;
        case 's':
            this.coordinates[0] = [{x:0,y:0},{x:1,y:0},{x:0,y:1},{x:-1,y:1}];
            this.coordinates[1] = [{x:0,y:0},{x:0,y:-1},{x:1,y:0},{x:1,y:1}];            
            this.color='#0B0B61';
        break;
        case 'z':
            this.coordinates[0] = [{x:0,y:0},{x:-1,y:0},{x:0,y:1},{x:1,y:1}];
            this.coordinates[1] = [{x:0,y:0},{x:0,y:-1},{x:-1,y:0},{x:-1,y:1}];            
            this.color='#610B5E';
        break;
        case 't':
            this.coordinates[0] = [{x:0,y:0},{x:1,y:0},{x:-1,y:0},{x:0,y:-1}];
            this.coordinates[1] = [{x:0,y:0},{x:1,y:0},{x:0,y:-1},{x:0,y:1}];
            this.coordinates[2] = [{x:0,y:0},{x:-1,y:0},{x:1,y:0},{x:0,y:1}];
            this.coordinates[3] = [{x:0,y:0},{x:0,y:-1},{x:0,y:1},{x:-1,y:0}];            
            this.color='#0B615E';
        break;
    } 
    
    
    this.paintGrig = function(){
        for(var i=0; i<=6; i++){
            this.cnv.beginPath();
            this.cnv.strokeStyle = "rgba(0,0,0,0.1)";
            this.cnv.lineWidth = 1;
            this.cnv.moveTo(0,i*this.cellWidth);
            this.cnv.lineTo(this.cellWidth*this.w,i*this.cellWidth);
            this.cnv.closePath();
            this.cnv.stroke();
        }
        for(var i=0; i<=6; i++){
            this.cnv.beginPath();
            this.cnv.strokeStyle = "rgba(0,0,0,0.1)"; 
            this.cnv.moveTo(i*this.cellWidth,0);
            this.cnv.lineTo(i*this.cellWidth,this.cellWidth*this.h);
            this.cnv.closePath(); 
            this.cnv.stroke();
        }
    }
    this.paintGrig();
    
      this.print = function(){
        for (var i in this.coordinates[this.version]){            
                this.rows[this.y+this.coordinates[this.version][i].y][this.x+this.coordinates[this.version][i].x] = this.color;                       
        }        
    }
     this.print ();

    this.clear = function(){
                    this.cnv.beginPath();     
                    this.cnv.clearRect ( 0 , 0 , 200 , 200 );
                    this.cnv.strokeStyle = "rgba(0, 0, 0, 0.3)";
                    this.cnv.fillStyle = "rgba(0, 0, 0, 0.3)";
                    this.cnv.fillRect(0,0,200,400);
                    this.cnv.closePath(); 
                    this.cnv.stroke();
                    this.cnv.fill();
    }
    this.paint = function(){
        this.clear();
        for (var i=0;i<this.h;i++){
            for (var j=0;j<this.w;j++){
                if (this.rows[i][j] != 0){
                    this.cnv.beginPath();
                    this.cnv.strokeStyle = "black";
                    this.cnv.fillStyle = this.rows[i][j];  
                    //cnv.fillRect((j)*this.cellWidth,(i-1)*this.cellWidth,(j-5)*this.cellWidth,(i)*this.cellWidth);
                    this.cnv.fillRect((j)*this.cellWidth,(i)*this.cellWidth,this.cellWidth,this.cellWidth);                     
                    this.cnv.closePath();
                    this.cnv.stroke();
                    this.cnv.fill();                    
                }
            }            
        }
        this.paintGrig();
    }
    this.paint ();
}            


function Figure(type,version,k){
    this.type=type;
    this.x=4;
    this.y=-2;
    this.color = '';
    this.version=version;
    this.coordinates = new Array();
    
    //for (var i=1;i<=4;i++){this.coordinates[i] = new Array();}
    switch (this.type){
        case 'i':
            this.coordinates[0] = [{x:0,y:0},{x:0,y:-1},{x:0,y:1},{x:0,y:2}];
            this.coordinates[1] = [{x:0,y:0},{x:1,y:0},{x:-1,y:0},{x:-2,y:0}];
            this.color='#FFDE99';
        break;
        case 'l':
            this.coordinates[0] = [{x:0,y:0},{x:0,y:-1},{x:0,y:1},{x:1,y:1}];
            this.coordinates[1] = [{x:0,y:0},{x:1,y:0},{x:-1,y:0},{x:-1,y:1}];
            this.coordinates[2] = [{x:0,y:0},{x:0,y:-1},{x:-1,y:-1},{x:0,y:1}];
            this.coordinates[3] = [{x:0,y:0},{x:-1,y:0},{x:1,y:0},{x:1,y:-1}];            
            this.color='#FFCE6B';
        break;
        case 'j':
            this.coordinates[0] = [{x:0,y:0},{x:0,y:-1},{x:0,y:1},{x:-1,y:1}];
            this.coordinates[1] = [{x:0,y:0},{x:-1,y:0},{x:-1,y:-1},{x:1,y:0}];
            this.coordinates[2] = [{x:0,y:0},{x:0,y:-1},{x:1,y:-1},{x:0,y:1}];
            this.coordinates[3] = [{x:0,y:0},{x:-1,y:0},{x:1,y:0},{x:1,y:1}];            
            this.color='#FFC144';
        break;
        case 'o':
            this.coordinates[0] = [{x:0,y:0},{x:1,y:0},{x:0,y:-1},{x:1,y:-1}];            
            this.color='#FFB725';
        break;
        case 's':
            this.coordinates[0] = [{x:0,y:0},{x:1,y:0},{x:0,y:1},{x:-1,y:1}];
            this.coordinates[1] = [{x:0,y:0},{x:0,y:-1},{x:1,y:0},{x:1,y:1}];            
            this.color='#FFAD09';
        break;
        case 'z':
            this.coordinates[0] = [{x:0,y:0},{x:-1,y:0},{x:0,y:1},{x:1,y:1}];
            this.coordinates[1] = [{x:0,y:0},{x:0,y:-1},{x:-1,y:0},{x:-1,y:1}];            
            this.color='#D79000';
        break;
        case 't':
            this.coordinates[0] = [{x:0,y:0},{x:1,y:0},{x:-1,y:0},{x:0,y:-1}];
            this.coordinates[1] = [{x:0,y:0},{x:1,y:0},{x:0,y:-1},{x:0,y:1}];
            this.coordinates[2] = [{x:0,y:0},{x:-1,y:0},{x:1,y:0},{x:0,y:1}];
            this.coordinates[3] = [{x:0,y:0},{x:0,y:-1},{x:0,y:1},{x:-1,y:0}];            
            this.color='#DAB05C';
        break;
    } 
    this.left = function(){
        this.clear();
        var left = true;
        for (var i in this.coordinates[this.version]){   
            if (this.x-1+this.coordinates[this.version][i].x<0){left=false;}
            try{ 
                if (field[k].rows[this.y+this.coordinates[this.version][i].y][this.x-1+this.coordinates[this.version][i].x] != 0){left = false}            
            } catch(e){}
        }         
        if (left){
            this.clear();
            this.x--;
            this.print();
            field[k].paint();
        } else {this.print}
    }
    this.right = function(){
        this.clear();
        var right = true;
        for (var i in this.coordinates[this.version]){ 
            if (this.x+1+this.coordinates[this.version][i].x>9){right=false;}
            try{ 
                if (field[k].rows[this.y+this.coordinates[this.version][i].y][this.x+1+this.coordinates[this.version][i].x] != 0){right = false}            
            } catch(e){}
        }   
        if (right){
            this.clear();
            this.x++;
            this.print();
            field[k].paint();
        } else {this.print}
    }
    this.nextVersion = function(){
        this.clear();
        var nexVersion = this.version+1;
        if (nexVersion > this.coordinates.length-1){nexVersion = 0;}
        var rotate = true;
        for (var i in this.coordinates[nexVersion]){                        
            if (this.y+this.coordinates[nexVersion][i].y==20){rotate=false;}
            if (this.x+this.coordinates[nexVersion][i].x>9){rotate=false;}
            if (this.x+this.coordinates[nexVersion][i].x<0){rotate=false;}
            else {
                try{ 
                    if (field[k].rows[this.y+this.coordinates[nexVersion][i].y][this.x+this.coordinates[nexVersion][i].x] != 0){
                        rotate = false; 
                    }            
                } catch(e){}
               
            }
        }
        
        if (rotate){
            this.clear();  
            this.version++;
            if (this.version > this.coordinates.length-1){this.version = 0;}
            this.print();
            field[k].paint();
        }
        else{
            this.print();
        }
    }
    this.print = function(){
        for (var i in this.coordinates[this.version]){
            var print=true;
            //console.log(this.coordinates[this.version][i].x,this.coordinates[this.version][i].y);
            if (this.y+this.coordinates[this.version][i].y<0){print=false;}
            if (this.y+this.coordinates[this.version][i].y>19){print=false;}
            if (this.x+this.coordinates[this.version][i].x<0){print=false;}
            if (this.x+this.coordinates[this.version][i].x>19){print=false;}
            //alert();
            //console.log(this.x+this.coordinates[this.version][i].x,' ',this.y+this.coordinates[this.version][i].y,print);
            if (print){
                field[k].rows[this.y+this.coordinates[this.version][i].y][this.x+this.coordinates[this.version][i].x] = this.color;
            }    
        }
        
    }
    this.clear = function(){
       for (var i in this.coordinates[this.version]){
            var print=true;
            if (this.y+this.coordinates[this.version][i].y<0){print=false;}
            if (this.y+this.coordinates[this.version][i].y>19){print=false;}
            if (this.x+this.coordinates[this.version][i].x<0){print=false;}
            if (this.x+this.coordinates[this.version][i].x>19){print=false;}
            //console.log(this.x+this.coordinates[this.version][i].x,' ',this.y+this.coordinates[this.version][i].y,print);
            if (print){
                field[k].rows[this.y+this.coordinates[this.version][i].y][this.x+this.coordinates[this.version][i].x] = 0;
            }    
        }
    }
    this.down = function(){        
        this.clear();              
        var fall = true;
        for (var i in this.coordinates[this.version]){                        
            if (this.y+this.coordinates[this.version][i].y+1==20){fall=false} 
            else {
                try{ 
                if (field[k].rows[this.y+this.coordinates[this.version][i].y+1][this.x+this.coordinates[this.version][i].x] != 0){fall = false}            
                } catch(e){}
               
            }
        }
        if (!fall){
                this.print();
                field[k].clearRows();
                $('#score'+k).html($('#maxscore').val()-field[k].score);
                 field[k].figureNumber++;
                if (field[k].figusreNumber>300){field[k].figureNumber=0}
                var newFigure = figures[field[k].figureNumber];                 
                figure[k] = new Figure(newFigure.figure,newFigure.version,k);
                
                var nextFigure = figures[field[k].figureNumber+1];                
                PaintNextFigure(nextFigure.figure,nextFigure.version,'nxtF1');
                if ( field[k].rows[0][3]!=0 || field[k].rows[0][4]!=0 || field[k].rows[0][5]!=0 || field[k].rows[0][6]!=0){
                   clearInterval(intrv1);                                      
                   $('#start').html('WIN<img height=25px; src="img/sl.png">');
                }
                if (field[k].score>=$('#maxscore').val()){
                    clearInterval(intrv1);                                   
                   $('#start').html('<img height=25px; src="img/sr.png"> WIN'); 
                }              
        } else {
            this.y++;
            this.print();
            
        }
        //this.print();
        field[k].paint();
        //field[k].logRows();
        //console.log('!');
    }    
    
}         


function playField(cnv){
    this.cnv=document.getElementById(cnv).getContext("2d"); 
    this.cellWidth = 30;
    this.w=10;
    this.h=20;
    this.score=0;
    this.rows = new Array();
    this.figureNumber=0;
    for (var i=0;i<this.h;i++){
        this.rows[i] = new Array();
        for (var j=0;j<this.w;j++){
            this.rows[i][j] = 0;
        }
    }
    this.randomFigure = function(){
        var f = ['i','l','j','o','s','z','t'];
        var fg = f[Math.floor(Math.random()*7)];        
        switch (fg){
            case 'i':
            case 's':
            case 'z':    
                var version = Math.floor(Math.random()*2);
            break;
            case 'l':
            case 'j':
            case 't':
                var version = Math.floor(Math.random()*4);
            break;
            case 'o':
                var version = 0;
            break;
        }
        return {figure : fg, version : version}
        
    };
    this.logRows = function(){
        for (var i=0;i<this.h;i++){
            var a = '';
            for (var j=0;j<this.w;j++){
                a+= this.rows[i][j];
            }
            console.log(a);
        }    
    }
    this.clear = function(){
                    this.cnv.beginPath();        
                    this.cnv.strokeStyle = "rgba(0,0,0,0.3)";
                    this.cnv.fillStyle = "rgba(0,0,0,0.3)";
                    this.cnv.clearRect ( 0,0,this.w*this.cellWidth,this.h*this.cellWidth );
                    this.cnv.fillRect(0,0,this.w*this.cellWidth,this.h*this.cellWidth);
                    this.cnv.closePath(); 
                    this.cnv.stroke();
                    this.cnv.fill();
    }
    this.paint = function(){
        this.clear();
        for (var i=0;i<this.h;i++){
            for (var j=0;j<this.w;j++){
                if (this.rows[i][j] != 0){
                    this.cnv.beginPath();
                    this.cnv.strokeStyle = "black";
                    this.cnv.fillStyle = this.rows[i][j];  
                    //cnv.fillRect((j)*this.cellWidth,(i-1)*this.cellWidth,(j-5)*this.cellWidth,(i)*this.cellWidth);
                    this.cnv.fillRect((j)*this.cellWidth,(i)*this.cellWidth,this.cellWidth,this.cellWidth);                     
                    this.cnv.closePath();
                    this.cnv.stroke();
                    this.cnv.fill();                    
                }
            }            
        }
        this.paintGrig();
    }
    this.paintGrig = function(){
        for(var i=0; i<=this.h; i++){
            this.cnv.beginPath();
            this.cnv.strokeStyle = "rgba(0,0,0,0.1)";
            this.cnv.lineWidth = 1;
            this.cnv.moveTo(0,i*this.cellWidth);
            this.cnv.lineTo(this.cellWidth*this.w,i*this.cellWidth);
            this.cnv.closePath();
            this.cnv.stroke();
        }
        for(var i=0; i<=this.w; i++){
            this.cnv.beginPath();
            this.cnv.strokeStyle = "rgba(0,0,0,0.1)"; 
            this.cnv.moveTo(i*this.cellWidth,0);
            this.cnv.lineTo(i*this.cellWidth,this.cellWidth*this.h);
            this.cnv.closePath(); 
            this.cnv.stroke();
        }
    }
    this.clearRows = function(){
        for (var i=0;i<this.h;i++){
            
            var full = true;
            for (var j=0;j<this.w;j++){
                if (this.rows[i][j]==0) {full = false}
            }
            
            if (full){
               this.score++;               
                for (var j=i;j>0;j--){
                    //this.rows[j] = new Array();
                    for (var k in this.rows[j-1])
                    this.rows[j][k] = this.rows[j-1][k];
                }
            }
            
        }
        
        if ((this.rows[0][2]!=0) || (this.rows[0][3]!=0) || (this.rows[0][4]!=0) || (this.rows[0][5]!=0)){
           // clearInterval(int);
         //   $('#info').css('display','block');
        }
        
    }
}

function timer(){

    figure[1].down();
  //   if (field[2].score>=$('#maxscore').val()){
    //               clearInterval(intrv1);
   //                 bot.stop();                  
    //                $('#start').html('WIN <img height=25px; src="img/sl.png">'); 
  //              }
   // figure[2].down();
    
}

var field = new Array();
field[1] = new playField('cnv');
field[1].paintGrig();
var figures = new Array();
var newFigure = figures[1];
var nextFigure = figures[2];
PaintNextFigure('',0,'nxtF1');

var start=false;

function stop(){
    clearInterval(int);
 //   bot.stop;
}

$('#start').click(function(){
  if (!start){      
      $('#start').html('STOP');   
     start=true;
 field = new Array();
field[1] = new playField('cnv');
//field[2] = new playField('cnv2');
field[1].paintGrig();
//field[2].paintGrig();

 figures = new Array();
for (i=-10; i<10000;i++){
    figures[i] = field[1].randomFigure();
}
field[1].figureNumber++;
 newFigure = figures[1];
 nextFigure = figures[2];
// newFigure2 = figures[0];
// nextFigure2 = figures[2];
//PaintNextFigure(nextFigure.figure,nextFigure.version,'nxtF1');
//PaintNextFigure(nextFigure2.figure,nextFigure2.version,'nxtF2');
//PaintNextFigure('i',0,'nxtF2');
 figure = new Array();
figure[1] = new Figure(newFigure.figure,newFigure.version,1);
//figure[2] = new Figure(newFigure2.figure,newFigure2.version,2);

field[1].paint();
//field[1].logRows();
//field[2].paint();
//field[2].logRows();
 //bot = new Bot();
//bot.generateWay();
      
   intrv1 = setInterval(timer,100);///////////////////////////////////SPEEEEED!!!!!!!!!!!!!!!!!!!
   //bot.speed=1000-$('#cs').val();
  // bot.nextFigure();
   PaintNextFigure(nextFigure.figure,nextFigure.version,'nxtF1');
  //  PaintNextFigure(nextFigure2.figure,nextFigure2.version,'nxtF2');
  } else {
     
      field[1] = new playField('cnv');
 //   field[2] = new playField('cnv2');
    field[1].paint();
   // field[2].paint();
    PaintNextFigure('',0,'nxtF1');
//PaintNextFigure('',0,'nxtF2');
  }
});




});